<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
		
	<h2>Sign Up Form</h2>

    <form action="/register" method="POST">
        @csrf

        <p>First name:</p>
        <input type="text" placeholder="Masukkan First Name" name="firstname">
        <p>Last name:</p>
        <input type="text" placeholder="Masukkan Last Name" name="lastname">
		<br>
		<br>
		<p>Gender :</p>
			<input type="radio" id="gender" name="gender" value="male">
			<label for="male">Male</label><br>
			<input type="radio" id="gender" name="gender" value="female">
			<label for="female">Female</label><br>
			<input type="radio" id="gender" name="gender" value="other">
			<label for="other">Others</label><br>
		<br>
		<br>
        <p>Nationality :</p>
				<select>
				<optgroup label="South East Asia">
				<option value="indonesia">Indonesia</option>
				<option value="malaysia">Malaysia</option>
				<option value="singapore">Singapore</option>
				<option value="thailand">Thailand</option>
				</optgroup>
			</select>
		<br>
		<br>
        <p>Language Spoken :</p>
			<input type="checkbox" id="language" name="language1" value="Indonesia">
  			<label for="vehicle1"> Bahasa Indonesia</label><br>
  			<input type="checkbox" id="language" name="language2" value="English">
  			<label for="vehicle2"> English</label><br>
  			<input type="checkbox" id="language" name="language3" value="Other">
  			<label for="vehicle3"> Other</label>
  		<br>
		<br>
        <p>Bio :</p>
  			<textarea cols="50" rows="5"></textarea>
		<br>
		<br>
		<input type="submit" value="register">
    </form>
</body>
</html>